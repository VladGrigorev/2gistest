<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rubric extends Model
{
    /**
     * Получить все компании, относящиеся к данной рубрике.
     */
    public function companies() {
        return $this->hasMany(Company::class);
    }
    
    /**
     * Получить все телефонные номера, приндлежащие данное фирме.
     */
    public function numbers() {
        return $this->hasManyThrough('App\Number', 'App\Company');
    }
}
