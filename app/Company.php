<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    /**
     * Получить все телефонные номера, приндлежащие данное фирме.
     */
    public function numbers() {
        return $this->hasMany(Number::class);
    }
}
