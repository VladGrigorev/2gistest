<?php

namespace App\Http\Controllers;

use Illuminate\Routing\ResponseFactory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class MainController extends Controller {

    public function __construct() {
        
    }
    
    public function mainPage() {
        return view('frontend.main');
    }
    
    public function documentationApi() {
        return view('frontend.documentation.api');
    }

}
