<?php

namespace App\Http\Controllers\Api;

use Illuminate\Routing\ResponseFactory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use App\Rubric;

class RubricsController extends ApiController {

    public function getCompanies($id) {
        if ($rubric = Rubric::find($id)) {
            return response()->json(
                            ['status' => 'ok', 'data' => ['companies' => $rubric->companies, 'numbers' => $rubric->numbers]], 200, [], JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT
            );
        } else {
            return response()->json(
                            'Not found rubric with this id', 404
            );
        }
    }

}
