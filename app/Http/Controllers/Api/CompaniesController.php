<?php

namespace App\Http\Controllers\Api;

use Illuminate\Routing\ResponseFactory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use App\Building;
use DB;

class CompaniesController extends ApiController {

    public function getCompaniesInLocation(Request $request) {
        $x = $request->input('x');
        $y = $request->input('y');
        $radius = $request->input('radius');
        $str = "ST_Distance(point(?,?),coordinates) <= ?";
        $buildings = Building::select(['id'])->whereRaw($str, [$x, $y, $radius])->get();

        $companies = $buildings->map(function ($building) {
            return ['companies' => $building->companies, 'numbers' => $building->numbers];
        });

        return response()->json(
                        ['status' => 'ok', 'data' => $companies], 200, [], JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT
        );
    }

}
