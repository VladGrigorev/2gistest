<?php

namespace App\Http\Controllers\Api;

use Illuminate\Routing\ResponseFactory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use App\Building;
use DB;

class BuildingsController extends ApiController {

    public function getCompanies($id) {
        if ($building = Building::find($id)) {
            return response()->json(
                            ['status' => 'ok', 'data' => ['companies' => $building->companies, 'numbers' => $building->numbers]], 200, [], JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT
            );
        } else {
            return response()->json(
                            'Not found building with this id', 404
            );
        }
    }

    public function getBuildings() {

        if ($buildings = Building::all(['id', 'address', DB::raw('X(coordinates) as latitude'),
                    DB::raw('Y(coordinates) as longitude')])) {
            return response()->json(
                            ['status' => 'ok', 'data' => $buildings], 200, [], JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT
            );
        } else {
            return response()->json(
                            'Not found building with this id', 404
            );
        }
    }

}
