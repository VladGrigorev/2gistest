<?php

use Illuminate\Http\Request;

/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | is assigned the "api" middleware group. Enjoy building your API!
  |
 */

/* Route::middleware('auth:api')->get('/user', function (Request $request) {
  return $request->user();
  }); */


Route::group(['namespace' => 'Api'], function () {
    Route::get('/buildings/{id}/companies', 'BuildingsController@getCompanies')->name('apiBulidingsCompanies');
    Route::get('/rubrics/{id}/companies', 'RubricsController@getCompanies')->name('apiRubricsCompanies');
    Route::post('/companies/companies-in-location', 'CompaniesController@getCompaniesInLocation')->name('companiesInLocation');
    Route::get('/buildings', 'BuildingsController@getBuildings')->name('apiBulidingsAll');
    /* Route::get('/api/companies/{id}', 'CompaniesController@show');
      Route::post('/api/companies', 'CompaniesController@store');
      Route::put('/api/companies/{id}', 'CompaniesController@update');
      Route::delete('/api/companies/{id}', 'CompaniesController@destroy'); */
});
