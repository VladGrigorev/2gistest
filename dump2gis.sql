-- MySQL dump 10.13  Distrib 5.7.18, for Linux (x86_64)
--
-- Host: localhost    Database: 2gis
-- ------------------------------------------------------
-- Server version	5.7.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `buildings`
--

DROP TABLE IF EXISTS `buildings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `buildings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address` varchar(512) DEFAULT NULL,
  `coordinates` point NOT NULL,
  PRIMARY KEY (`id`),
  SPATIAL KEY `coordinates` (`coordinates`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `buildings`
--

LOCK TABLES `buildings` WRITE;
/*!40000 ALTER TABLE `buildings` DISABLE KEYS */;
INSERT INTO `buildings` VALUES (1,'Олимпия, Новосибирск, Галущака, 2а','\0\0\0\0\0\0\0�{b��K@�\�:q9�T@'),(2,'Новосибирск, Станционная, 20/1 к2','\0\0\0\0\0\0\0F\\\0\Z�K@�\�4ҶT@'),(3,'Новосибирск, Титова, 26','\0\0\0\0\0\0\0z\�c�\�|K@�x\r��T@'),(4,'Новосибирск, Планировочная, 16а к2','\0\0\0\0\0\0\0�s\�^K@ӈ�}�T@'),(5,'Новосибирск, Бориса Богаткова, 73','\0\0\0\0\0\0\0��n/i�K@��\�u�T@'),(6,'Новосибирск, Семьи Шамшиных, 42','\0\0\0\0\0\0\0N\�}��K@?s֧�T@'),(7,'Новосибирск, Академика Лаврентьева проспект, 6','\0\0\0\0\0\0\0��	mK@\��L�\�T@'),(8,'Новосибирск, Большевистская, 276/1','\0\0\0\0\0\0\0�=	l\�}K@g���T@');
/*!40000 ALTER TABLE `buildings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `companies`
--

DROP TABLE IF EXISTS `companies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `companies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(512) DEFAULT NULL,
  `building_id` int(11) NOT NULL,
  `rubric_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_companys_building1_idx` (`building_id`),
  KEY `fk_companies_rubric1_idx` (`rubric_id`),
  CONSTRAINT `fk_companies_rubrics1` FOREIGN KEY (`rubric_id`) REFERENCES `rubrics` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_companys_buildings1` FOREIGN KEY (`building_id`) REFERENCES `buildings` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `companies`
--

LOCK TABLES `companies` WRITE;
/*!40000 ALTER TABLE `companies` DISABLE KEYS */;
INSERT INTO `companies` VALUES (1,'Автоскорая Помощь Сибири 54, служба эвакуации автомобилей',1,4),(2,'АВТОВАЛ, служба эвакуации',2,4),(3,'АБ АБАРДАЖ\r\nСлужба эвакуации',3,4),(4,'АвтоПартс\r\nКомпания по эвакуации автомобилей',4,4),(5,'АвтоМощь\r\nКомпания мобильного шиномонтажа и помощи на дорогах',5,4),(6,'Скорая медицинская помощь',6,5),(7,'Болит спина?\r\nМедицинский кабинет',7,5),(8,'Межрайонный отдел технического надзора и регистрации автомототранспортных средств ГИБДД №4',8,7);
/*!40000 ALTER TABLE `companies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `numbers`
--

DROP TABLE IF EXISTS `numbers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `numbers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number` varchar(128) DEFAULT NULL,
  `company_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_numbers_company_idx` (`company_id`),
  CONSTRAINT `fk_numbers_company` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `numbers`
--

LOCK TABLES `numbers` WRITE;
/*!40000 ALTER TABLE `numbers` DISABLE KEYS */;
INSERT INTO `numbers` VALUES (1,'+73833311377',1),(2,'+79139139977',2),(3,'+7–952–929–07–07',5),(4,'+7–913–929–07–47',5),(5,'+7–923–105–47–47',5),(6,'+7–905–936–62–45',5),(7,'+7–913–900–16–96',5),(8,'+7 (383) 286–06–09',4),(9,'+7 (383) 292–50–69',3),(10,'+7 (383) 328–00–33',8),(11,'+7 (383) 328–00–34',8),(12,'103',6);
/*!40000 ALTER TABLE `numbers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rubrics`
--

DROP TABLE IF EXISTS `rubrics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rubrics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(512) DEFAULT NULL,
  `parent_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_rubric_rubric1_idx` (`parent_id`),
  CONSTRAINT `fk_rubric_rubric1` FOREIGN KEY (`parent_id`) REFERENCES `rubrics` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rubrics`
--

LOCK TABLES `rubrics` WRITE;
/*!40000 ALTER TABLE `rubrics` DISABLE KEYS */;
INSERT INTO `rubrics` VALUES (1,'main',1),(2,'Аварийные / справочные / экстренные службы',1),(3,'Автосервис / Автотовары',1),(4,'Эвакуация автомобилей',2),(5,'Скорая медицинская помощь',2),(6,'Город / Власть',1),(7,'ГИБДД',6);
/*!40000 ALTER TABLE `rubrics` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-07-13 15:02:34
