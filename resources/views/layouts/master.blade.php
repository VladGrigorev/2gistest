<html>
    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@yield('title')</title>

        <!-- Material Design fonts -->
        <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700">
        <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/icon?family=Material+Icons">

        <!-- Bootstrap -->
        <link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700' rel='stylesheet' type='text/css'>

        <!-- Bootstrap Material Design -->
        {{--<link rel="stylesheet" type="text/css" href="/css/bootstrap-material-design.css">--}}
        <!-- <link rel="stylesheet" type="text/css" href="/css/ripples.min.css"> -->

        <!-- bxSlider Javascript file 
        <script src="{{asset('/js/jquery.bxslider.min.js')}}"></script>-->
        <!-- bxSlider CSS file         -->
        <link rel="stylesheet" type="text/css" href="/css/main.css">
        <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>-->
        <script src="{{asset('https://unpkg.com/babel-standalone@6/babel.min.js')}}"></script>
        <script>
            window.Laravel = {!! json_encode([
                'csrfToken' => csrf_token(),
            ]) !!};
        </script>
        <script src="https://maps.api.2gis.ru/2.0/loader.js?pkg=full"></script>
    </head>  
    <body class="bg-white">
        <div class='wrapper'>
            <div class='sidebar'>
                <div class='title'>
                    2GisTest
                </div>
                <ul class='nav'>
                    <li>
                        <a>Dashboard</a>
                    </li>
                    <li>
                        <a href="{{route('main')}}">Главная</a>
                    </li>
                    <li>
                        <a href="{{route('documentation.api')}}">Документация Api</a>
                    </li>

                </ul>
            </div>
            
            <div class='content isOpen'>
                @yield('content')
            </div>
            {{ csrf_field() }}
        </div>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script src="js/main.js"></script>
        @yield('scripts')
        <footer>

        </footer>
    </body>
</html>