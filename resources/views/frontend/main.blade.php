@extends('layouts.master')

@section('title', 'Main')

@section('content')
<div id="map"></div>
<script>
    let map;
    let novosibirsk = [55.0242968, 82.9501523];
    DG.then(function () {
        map = DG.map('map', {
            center: novosibirsk,
            zoom: 12
        });
        getBulidings().then(
                markers => {
                    markers.forEach(function (item) {
                        getCompaniesFromBuilding(item.id).then(
                                companies => {
                                    console.log(companies);
                                    let companiesTitle = "";
                                    let companiesNumbers = "";
                                    companies['companies'].forEach(function (company) {
                                        companiesTitle += company.title+"<br>";
                                    });
                                    companies['numbers'].forEach(function (number) {
                                        companiesNumbers += number.number+"<br>";
                                    });
                                    DG.marker([item.latitude, item.longitude]).addTo(map).bindPopup(companiesTitle + companiesNumbers + item.address);
                                });
                    });
                },
                error => console.log(`Rejected: ${error}`)
        );
    });
    function getCompaniesFromBuilding(id) {
        return new Promise(function (resolve, reject) {
            let url = '{{ route("apiBulidingsCompanies", ":id") }}';
            url = url.replace(':id', id);
            fetch(url, {
                method: 'get',
                credentials: "same-origin",
                dataType: 'json',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
            })
                    .then((response) => {
                        if (response.ok) {
                            console.log('success!');
                            return response.json();
                        }
                        return response.json();
                    })
                    .then((json) => {
                        if (json['status'] == 'ok') {
                            let companies = json['data'];
                            resolve(companies);
                        }
                    })
                    .catch((error) => {
                        reject(error);
                    });
        });
    }
    function getBulidings() {
        return new Promise(function (resolve, reject) {
            fetch("{{route('apiBulidingsAll')}}", {
                method: 'get',
                credentials: "same-origin",
                dataType: 'json',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
            })
                    .then((response) => {
                        if (response.ok) {
                            console.log('success!');
                            return response.json();
                        }
                        return response.json();
                    })
                    .then((json) => {
                        if (json['status'] == 'ok') {
                            console.log(json);
                            let markers = json['data'];
                            resolve(markers);
                        }
                    })
                    .catch((error) => {
                        reject(error);
                    });
        });
    }

</script>
@endsection
