@extends('layouts.master')

@section('title', 'Main')

@section('content')
<table class="table table-bordered" style="width:65%">
    <thead>
        <tr>
            <th>#</th>
            <th>HTTP метод</th>
            <th>URL</th>
            <th>Параметры</th>
            <th>Возвращаемое значение</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <th scope="row">1</th>
            <td>GET</td>
            <td>/api/buildings/{id}/companies</td>
            <td>
                <div>
                    <h5>Параметр</h5>
                    id
                </div>
                <div>
                    <h5>Описание</h5>
                    id здания
                </div>
                <div>
                    <h5>Тип параметра</h5>
                    path
                </div>
                <div>
                    <h5>Тип передаваемых данных</h5>
                    integer
                </div>          
            </td>
            <td>
                Возвращает список компаний, находящихся в указанном здании, а также список номеров компаний.
                <pre>                
                {
                    "status": "ok",
                    "data": {
                        "companies": [
                            {
                                "id": 1,
                                "title": "Автоскорая Помощь Сибири 54, служба эвакуации автомобилей",
                                "building_id": 1,
                                "rubric_id": 4
                            }
                        ],
                        "numbers": [
                            {
                                "id": 1,
                                "number": "+73833311377",
                                "company_id": 1,
                                "building_id": 1
                            }
                        ]
                    }
                }
                </pre>
            </td>
        </tr>
        <tr>
            <th scope="row">2</th>
            <td>GET</td>
            <td>/api/rubrics/{id}/companies</td>
            <td>
                <div>
                    <h5>Параметр</h5>
                    id
                </div>
                <div>
                    <h5>Описание</h5>
                    id рубрики
                </div>
                <div>
                    <h5>Тип параметра</h5>
                    path
                </div>
                <div>
                    <h5>Тип передаваемых данных</h5>
                    integer
                </div>  
            </td>
            <td>
                Возвращает список организаций относяшихся к данной рубрике
                <pre>
                {
                    "status": "ok",
                    "data": {
                        "companies": [
                            {
                                "id": 1,
                                "title": "Автоскорая Помощь Сибири 54, служба эвакуации автомобилей",
                                "building_id": 1,
                                "rubric_id": 4
                            },
                            {
                                "id": 2,
                                "title": "АВТОВАЛ, служба эвакуации",
                                "building_id": 2,
                                "rubric_id": 4
                            },
                            {
                                "id": 3,
                                "title": "АБ АБАРДАЖ\r\nСлужба эвакуации",
                                "building_id": 3,
                                "rubric_id": 4
                            }
                        ],
                        "numbers": [
                            {
                                "id": 1,
                                "number": "+73833311377",
                                "company_id": 1,
                                "rubric_id": 4
                            },
                            {
                                "id": 2,
                                "number": "+79139139977",
                                "company_id": 2,
                                "rubric_id": 4
                            }
                        ]
                    }
                }
                </pre>
            </td>
        </tr>
        <tr>
            <th scope="row">3</th>
            <td>POST</td>
            <td>api/companies/companies-in-location</td>
            <td>
                <div>
                    <h5>Параметр</h5>
                    x
                </div>
                <div>
                    <h5>Описание</h5>
                    точка абсцисс
                </div>
                <div>
                    <h5>Тип параметра</h5>
                    body
                </div>
                <div>
                    <h5>Тип передаваемых данных</h5>
                    point
                </div> 
                <hr>
                <div>
                    <h5>Параметр</h5>
                    y
                </div>
                <div>
                    <h5>Описание</h5>
                    точка ординат 
                </div>
                <div>
                    <h5>Тип параметра</h5>
                    body
                </div>
                <div>
                    <h5>Тип передаваемых данных</h5>
                    point
                </div> 
                <hr>
                <div>
                    <h5>Параметр</h5>
                    radius
                </div>
                <div>
                    <h5>Описание</h5>
                    радиус
                </div>
                <div>
                    <h5>Тип параметра</h5>
                    body
                </div>
                <div>
                    <h5>Тип передаваемых данных</h5>
                    integer
                </div> 
            </td>
            <td>
                Возвращает список организаций, которые находятся в указанном радиусе относительно указанной точке на карте
                <pre>  
                {
                    "status": "ok",
                    "data": [
                        {
                            "companies": [
                                {
                                    "id": 1,
                                    "title": "Автоскорая Помощь Сибири 54, служба эвакуации автомобилей",
                                    "building_id": 1,
                                    "rubric_id": 4
                                }
                            ],
                            "numbers": [
                                {
                                    "id": 1,
                                    "number": "+73833311377",
                                    "company_id": 1,
                                    "building_id": 1
                                }
                            ]
                        },
                        {
                            "companies": [
                                {
                                    "id": 2,
                                    "title": "АВТОВАЛ, служба эвакуации",
                                    "building_id": 2,
                                    "rubric_id": 4
                                }
                            ],
                            "numbers": [
                                {
                                    "id": 2,
                                    "number": "+79139139977",
                                    "company_id": 2,
                                    "building_id": 2
                                }
                            ]
                        }
                    ]
                }
                </pre>  
            </td>
        </tr>
        <tr>
            <th scope="row">4</th>
            <td>GET</td>
            <td>/api/buildings</td>
            <td>-</td>
            <td>
                Возвращает всех зданий.
                <pre>                
                {
                    "status": "ok",
                    "data": [
                        {
                            "id": 1,
                            "address": "Олимпия, Новосибирск, Галущака, 2а",
                            "latitude": 55.051678,
                            "longitude": 82.894131
                        },
                        {
                            "id": 2,
                            "address": "Новосибирск, Станционная, 20\/1 к2",
                            "latitude": 54.997226,
                            "longitude": 82.85658
                        },
                        {
                            "id": 3,
                            "address": "Новосибирск, Титова, 26",
                            "latitude": 54.975668,
                            "longitude": 82.827945
                        }
                    ]
                }
                </pre>
            </td>
        </tr>
    </tbody>
</table>
@endsection
